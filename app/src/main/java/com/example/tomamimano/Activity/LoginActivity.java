package com.example.tomamimano.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.tomamimano.MainActivity;
import com.example.tomamimano.R;
import com.example.tomamimano.VolleyRP;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    private static String IP = "https://yoteapoyo.000webhostapp.com/ArchivosPhp/Login_GETID.php?id=";
    private EditText et_Usuario;
    private EditText et_Contraseña;
    private Button btn_Ingresar;
    private RequestQueue mRequest;
    private VolleyRP volley;
    private String USER = "";
    private String PASSWORD = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();

        et_Usuario = findViewById(R.id.et_Usuario);
        et_Contraseña = findViewById(R.id.et_Contasena);
        btn_Ingresar = findViewById(R.id.btn_Ingresar);

        btn_Ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VerificarLogin(et_Usuario.getText().toString().toLowerCase(), et_Contraseña.getText().toString().toLowerCase());
            }
        });
    }

    public void VerificarLogin(String user, String password) {
        USER = user;
        PASSWORD = password;
        SolicitudJSON(IP + user);
    }

    public void SolicitudJSON(String URL) {
        JsonObjectRequest solicitud = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject datos) {
                VerificarPassword(datos);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this, "Ocurrio un error, por favor contactese con el administrador", Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud, mRequest, this, volley);
    }


    public void VerificarPassword(JSONObject datos) {
        try {
            String estado = datos.getString("resultado");
            if (estado.equals("CC")) {
                JSONObject Jsondatos = new JSONObject(datos.getString("datos"));
                String usuario = Jsondatos.getString("id").toLowerCase();
                String contraseña = Jsondatos.getString("Password").toLowerCase();
                if (usuario.equals(USER) && contraseña.equals(PASSWORD)) {
                    Toast.makeText(this, "Usted se ha logueado correctamente", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(this, MainActivity.class);
                    startActivity(i);
                } else
                    Toast.makeText(this, "Usuario o contraseña incorrecta", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, estado, Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
        }
    }
}