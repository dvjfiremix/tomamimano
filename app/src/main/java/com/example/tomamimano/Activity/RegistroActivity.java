package com.example.tomamimano.Activity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.tomamimano.Entidades.Firebase.Usuario;
import com.example.tomamimano.Fragments.Icons.FemaleFragment;
import com.example.tomamimano.Fragments.Icons.ListaPersonajesFragment;
import com.example.tomamimano.Fragments.Icons.TabbedDialog;
import com.example.tomamimano.R;
import com.example.tomamimano.Utilidades.Constantes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;

import Library.Utils;
import de.hdodenhof.circleimageview.CircleImageView;

import static Library.Globals.FOTO;

public class RegistroActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener,ListaPersonajesFragment.PhotoClick, FemaleFragment.PhotoClick{

    private CircleImageView fotoPerfil;
    private EditText txtNombre;
    private EditText txtCorreo;
    private EditText txtContraseña;
    private EditText txtContraseñaRepetida;
    private EditText txtFechaDeNacimiento;
    private Button btnRegistrar;
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private Utils utils;
    private RadioGroup generos;
    private RadioGroup usuarios;

    private String fechaDeNacimiento,genero = "Hombre",tipo = "Guía";
    private TabbedDialog dialogFragment;
    Calendar now = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        fotoPerfil = findViewById(R.id.fotoPerfil);
        ListaPersonajesFragment f = new ListaPersonajesFragment();
        txtNombre = findViewById(R.id.idRegistroNombre);
        txtCorreo = findViewById(R.id.idRegistroCorreo);
        txtContraseña = findViewById(R.id.idRegistroContraseña);
        txtContraseñaRepetida = findViewById(R.id.idRegistroContraseñaRepetida);
        txtFechaDeNacimiento = findViewById(R.id.txtFechaDeNacimiento);
        btnRegistrar = findViewById(R.id.idRegistroRegistrar);
        generos = findViewById(R.id.rgGenero);
        usuarios = findViewById(R.id.rgUsuario);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        fotoPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                // Create and show the dialog.
                dialogFragment = new TabbedDialog();
                dialogFragment.show(ft,"dialog");

            }
        });

        generos.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.male:
                        genero = "Hombre";
                        break;
                    case R.id.female:
                        genero = "Mujer";
                        break;
                }
            }
        });

        usuarios.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.participant:
                        tipo = "Participante";
                        break;
                    case R.id.guia:
                        tipo = "Guía";
                        break;
                }
            }
        });

        txtFechaDeNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDailog();
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String correo = txtCorreo.getText().toString();
                final String nombre = txtNombre.getText().toString();
                if(isValidEmail(correo) && validarContraseña() && validarNombre(nombre)){
                    String contraseña = txtContraseña.getText().toString();
                    mAuth.createUserWithEmailAndPassword(correo, contraseña)
                            .addOnCompleteListener(RegistroActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Toast.makeText(RegistroActivity.this, "Se registro correctamente.", Toast.LENGTH_SHORT).show();
                                        Usuario usuario = new Usuario();
                                        usuario.setCorreo(correo);
                                        usuario.setNombre(nombre);
                                        usuario.setGenero(genero);
                                        usuario.setTipo(tipo);
                                        if(!utils.getShared(FOTO,"").equals("")){
                                            usuario.setFotoPerfilURL(utils.getShared(FOTO,""));
                                        }
                                        else{
                                            usuario.setFotoPerfilURL(Constantes.URL_FOTO_POR_DEFECTO_USUARIOS);
                                        }

                                        usuario.setFechaDeNacimiento(fechaDeNacimiento);
                                        FirebaseUser currentUser = mAuth.getCurrentUser();
                                        DatabaseReference reference = database.getReference("Usuarios/"+currentUser.getUid());
                                        reference.setValue(usuario);
                                        finish();
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Toast.makeText(RegistroActivity.this, "Error al registrarse.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }else{
                    Toast.makeText(RegistroActivity.this, "Validaciones funcionando.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean validarContraseña(){
        String contraseña,contraseñaRepetida;
        contraseña = txtContraseña.getText().toString();
        contraseñaRepetida = txtContraseñaRepetida.getText().toString();
        if(contraseña.equals(contraseñaRepetida)){
            if(contraseña.length()>=6 && contraseña.length()<=16){
                return true;
            }else return false;
        }else return false;
    }

    public boolean validarNombre(String nombre){
        return !nombre.isEmpty();
    }

    private void showDatePickerDailog(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                this,
                now.get(Calendar.YEAR)-36,
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        now.set(Calendar.YEAR, year);
        now.set(Calendar.MONTH, month);
        now.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String mes,days;
        switch (month) {
            case 0:
                mes = "01";
                break;
            case 1:
                mes = "02";
                break;
            case 2:
                mes = "03";
                break;
            case 3:
                mes = "04";
                break;
            case 4:
                mes = "05";
                break;
            case 5:
                mes = "06";
                break;
            case 6:
                mes = "07";
                break;
            case 7:
                mes = "08";
                break;
            case 8:
                mes = "09";
                break;
            case 9:
                mes = "10";
                break;
            case 10:
                mes = "11";
                break;
            case 11:
                mes = "12";
                break;
            default:
                mes = "01";
                break;
        }
        if (dayOfMonth < 10) {
            days = "0" + dayOfMonth;
        } else {
            days = String.valueOf(dayOfMonth);
        }
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/mm/yyyy", Locale.getDefault());
        //Date date = now.getTime();
        fechaDeNacimiento = days + "/" + mes + "/" + year;
        txtFechaDeNacimiento.setText(fechaDeNacimiento);

    }

    @Override
    public void onResume(){
        utils = Utils.getInstance(this);
        if(!utils.getShared(FOTO,"").equals("")){
            Glide.with(this).load(utils.getImage(utils.getShared(FOTO,""),this)).into(fotoPerfil);
        }
        else {
            Glide.with(this).load(Constantes.URL_FOTO_POR_DEFECTO_USUARIOS).into(fotoPerfil);
        }
        super.onResume();
    }

    @Override
    public void onBackPressed(){
        utils.SaveShared(FOTO,"");
        super.onBackPressed();
    }

    @Override
    public void onDestroy(){
        utils.SaveShared(FOTO,"");
        super.onDestroy();
    }

    @Override
    public void ClickPhoto() {
        dialogFragment.dismiss();
        Glide.with(this).load(utils.getImage(utils.getShared(FOTO, ""), this)).into(fotoPerfil);
    }
}
