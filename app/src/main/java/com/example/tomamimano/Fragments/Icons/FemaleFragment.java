package com.example.tomamimano.Fragments.Icons;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tomamimano.Activity.RegistroActivity;
import com.example.tomamimano.R;

import java.util.ArrayList;

import Library.Utils;

import static Library.Globals.FOTO;

public class FemaleFragment extends Fragment {

    RecyclerView recyclerPersonajes;
    ArrayList<PersonajeVo> listaPersonajes;
    Context contexto;
    Utils utils;
    private PhotoClick listener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_lista_personajes, container, false);

        contexto = getContext();

        listaPersonajes = new ArrayList<>();
        recyclerPersonajes = vista.findViewById(R.id.RecyclerId);
        recyclerPersonajes.setLayoutManager(new GridLayoutManager(getContext(), 2));

        llenarLista();

        AdaptadorPersonajes adapter = new AdaptadorPersonajes(listaPersonajes);

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(contexto, RegistroActivity.class);
                utils.SaveShared(FOTO, utils.ObtenerImagenMujer(listaPersonajes.get
                        (recyclerPersonajes.getChildAdapterPosition(view))
                        .getNombre()));
                if (getActivity() instanceof PhotoClick) {
                    listener.ClickPhoto();
                }
            }
        });
        recyclerPersonajes.setAdapter(adapter);

        return vista;
    }


    private void llenarLista() {
        listaPersonajes.add(new PersonajeVo("A1", "", R.drawable.user_female_blue_black));
        listaPersonajes.add(new PersonajeVo("A2", "", R.drawable.user_female_blue_brown));
        listaPersonajes.add(new PersonajeVo("A3", "", R.drawable.user_female_blue_gray));
        listaPersonajes.add(new PersonajeVo("A4", "", R.drawable.user_female_blue_red));
        listaPersonajes.add(new PersonajeVo("A5", "", R.drawable.user_female_blue_yellow));
        listaPersonajes.add(new PersonajeVo("G1", "", R.drawable.user_female_green_black));
        listaPersonajes.add(new PersonajeVo("G2", "", R.drawable.user_female_green_brown));
        listaPersonajes.add(new PersonajeVo("G3", "", R.drawable.user_female_green_gray));
        listaPersonajes.add(new PersonajeVo("G4", "", R.drawable.user_female_green_red));
        listaPersonajes.add(new PersonajeVo("G5", "", R.drawable.user_female_green_yellow));
        listaPersonajes.add(new PersonajeVo("P1", "", R.drawable.user_female_purple_black));
        listaPersonajes.add(new PersonajeVo("P2", "", R.drawable.user_female_purple_brown));
        listaPersonajes.add(new PersonajeVo("P3", "", R.drawable.user_female_purple_gray));
        listaPersonajes.add(new PersonajeVo("P4", "", R.drawable.user_female_purple_red));
        listaPersonajes.add(new PersonajeVo("P5", "", R.drawable.user_female_purple_yellow));
    }

    @Override
    public void onResume() {
        utils = Utils.getInstance(getActivity());
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof PhotoClick) {
            listener = (PhotoClick) activity;
        }
        super.onAttach(activity);
    }

    public interface PhotoClick{
        void ClickPhoto();
    }

}

