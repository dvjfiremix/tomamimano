package com.example.tomamimano.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.tomamimano.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import de.hdodenhof.circleimageview.CircleImageView;

public class MiPerfilFragment extends Fragment {
    private static final int PHOTO_PERFIL = 2;
    View vista;
    private CircleImageView cambiarFotoPerfil;
    private CircleImageView fotoPerfil;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private Button Cerrar_sesion;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        vista = inflater.inflate(R.layout.fragment_miperfil, container, false);

        storage = FirebaseStorage.getInstance();
        Cerrar_sesion = vista.findViewById(R.id.Cerrar_sesion);
        fotoPerfil = vista.findViewById(R.id.fotoPerfil);
        cambiarFotoPerfil = vista.findViewById(R.id.cambiarFotoPerfil);
        cambiarFotoPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/jpeg");
                i.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                getActivity().startActivityForResult(Intent.createChooser(i, "Selecciona una foto"), PHOTO_PERFIL);
            }
        });
        Cerrar_sesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
            }
        });
        return vista;
    }
}