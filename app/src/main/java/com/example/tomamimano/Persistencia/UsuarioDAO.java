package com.example.tomamimano.Persistencia;

import android.support.annotation.NonNull;

import com.example.tomamimano.Entidades.Firebase.Usuario;
import com.example.tomamimano.Entidades.Logica.LUsuario;
import com.example.tomamimano.Utilidades.Constantes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO {

    private static UsuarioDAO usuarioDAO;
    private FirebaseDatabase database;
    private DatabaseReference referenceUsuarios;

    private UsuarioDAO() {
        database = FirebaseDatabase.getInstance();
        referenceUsuarios = database.getReference(Constantes.NODO_USUARIOS);
    }

    public static UsuarioDAO getInstancia() {
        if (usuarioDAO == null) usuarioDAO = new UsuarioDAO();
        return usuarioDAO;

    }

    public static String getKeyUsuario() {
        return FirebaseAuth.getInstance().getUid();
    }

    public long fechaDeCreacionLong() {
        return FirebaseAuth.getInstance().getCurrentUser().getMetadata().getCreationTimestamp();
    }

    public long fechaDeUltimaVezQueSeLogeoLong() {
        return FirebaseAuth.getInstance().getCurrentUser().getMetadata().getLastSignInTimestamp();
    }

    public void añadirFotoDePerfilALosUsuariosQueNoTienenFoto() {
        referenceUsuarios.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<LUsuario> lUsuariosLista = new ArrayList<>();
                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                    Usuario usuario = childDataSnapshot.getValue(Usuario.class);
                    LUsuario lUsuario = new LUsuario(childDataSnapshot.getKey(), usuario);
                    lUsuariosLista.add(lUsuario);
                }

                for (LUsuario lUsuario : lUsuariosLista) {
                    if (lUsuario.getUsuario().getFotoPerfilURL() == null) {
                        referenceUsuarios.child(lUsuario.getKey()).child("fotoPerfilURL").setValue(Constantes.URL_FOTO_POR_DEFECTO_USUARIOS);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
