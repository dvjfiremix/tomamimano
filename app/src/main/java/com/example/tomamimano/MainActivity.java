package com.example.tomamimano;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.tomamimano.Fragments.ImportFragment;
import com.example.tomamimano.Fragments.MasFragment;
import com.example.tomamimano.Fragments.MensajesFragment;
import com.example.tomamimano.Fragments.MiPerfilFragment;
import com.example.tomamimano.Fragments.PodcastFragment;
import com.example.tomamimano.Fragments.VideosFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (id == R.id.nav_inicio) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, new ImportFragment()).commit();
        } else if (id == R.id.nav_mensajes) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, new MensajesFragment()).commit();
        } else if (id == R.id.nav_videos) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, new VideosFragment()).commit();
        } else if (id == R.id.nav_podcast) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, new PodcastFragment()).commit();
        } else if (id == R.id.nav_mas) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, new MasFragment()).commit();
        } else if (id == R.id.nav_miperfil) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, new MiPerfilFragment()).commit();
        } else if (id == R.id.nav_foros) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, new MiPerfilFragment()).commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
