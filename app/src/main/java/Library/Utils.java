package Library;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import com.example.tomamimano.R;

import static Library.Globals.SHARED_APP;

public class Utils extends AppCompatActivity {
    private static Utils util;
    private SharedPreferences sharedPreferences;

    private Utils(Context context) {
        sharedPreferences = context.getSharedPreferences(SHARED_APP, Context.MODE_PRIVATE);
    }

    public static Utils getInstance(Context context) {
        if (context != null) {
            util = new Utils(context);
        }
        return util;
    }

    //Obtiene las imagenes de las muejeres
    public int getImage(String imagen, Activity activity) {
        int drawable;
        switch (imagen) {
            case "user_male_blue_black":
                drawable = R.drawable.user_male_blue_black;
                break;

            case "user_male_blue_brown":
                drawable = R.drawable.user_male_blue_brown;
                break;

            case "user_male_blue_gray":
                drawable = R.drawable.user_male_blue_gray;
                break;

            case "user_male_blue_red":
                drawable = R.drawable.user_male_blue_red;
                break;

            case "user_male_blue_yellow":
                drawable = R.drawable.user_male_blue_yellow;
                break;

            case "user_male_green_black":
                drawable = R.drawable.user_male_green_black;
                break;

            case "user_male_green_brown":
                drawable = R.drawable.user_male_green_brown;
                break;

            case "user_male_green_gray":
                drawable = R.drawable.user_male_green_gray;
                break;

            case "user_male_green_red":
                drawable = R.drawable.user_male_green_red;
                break;

            case "user_male_green_yellow":
                drawable = R.drawable.user_male_green_yellow;
                break;

            case "user_male_red_black":
                drawable = R.drawable.user_male_red_black;
                break;

            case "user_male_red_brown":
                drawable = R.drawable.user_male_red_brown;
                break;

            case "user_male_red_gray":
                drawable = R.drawable.user_male_red_gray;
                break;

            case "user_male_red_red":
                drawable = R.drawable.user_male_red_red;
                break;

            case "user_male_red_yellow":
                drawable = R.drawable.user_male_red_yellow;
                break;

            case "user_female_blue_black":
                drawable = R.drawable.user_female_blue_black;
                break;

            case "user_female_blue_brown":
                drawable = R.drawable.user_female_blue_brown;
                break;

            case "user_female_blue_gray":
                drawable = R.drawable.user_female_blue_gray;
                break;

            case "user_female_blue_red":
                drawable = R.drawable.user_female_blue_red;
                break;

            case "user_female_blue_yellow":
                drawable = R.drawable.user_female_blue_yellow;
                break;

            case "user_female_green_black":
                drawable = R.drawable.user_female_green_black;
                break;

            case "user_female_green_brown":
                drawable = R.drawable.user_female_green_brown;
                break;

            case "user_female_green_gray":
                drawable = R.drawable.user_female_green_gray;
                break;

            case "user_female_green_red":
                drawable = R.drawable.user_female_green_red;
                break;

            case "user_female_green_yellow":
                drawable = R.drawable.user_female_green_yellow;
                break;

            case "user_female_purple_black":
                drawable = R.drawable.user_female_purple_black;
                break;

            case "user_female_purple_brown":
                drawable = R.drawable.user_female_purple_brown;
                break;

            case "user_female_purple_gray":
                drawable = R.drawable.user_female_purple_gray;
                break;

            case "user_female_purple_red":
                drawable = R.drawable.user_female_purple_red;
                break;

            case "user_female_purple_yellow":
                drawable = R.drawable.user_female_purple_yellow;
                break;

            default:
                drawable = R.drawable.user_male_red_yellow;
                break;
        }
        return drawable;
    }

    //Guarda las preferencias seleccionadas
    public boolean SaveShared(String key, String value) {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, value);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    //Obtienes las preferencias
    public String getShared(String key, String value) {
        String imagen = "";
        try {
            imagen = sharedPreferences.getString(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imagen;
    }

    //Obtener nombre de imagen de Hombre
    public String ObtenerImagenHombre(String nombre){
        String image = "";
        switch (nombre){
            case "A1":
                image = "user_male_blue_black";
                break;

            case "A2":
                image = "user_male_blue_brown";
                break;

            case "A3":
                image = "user_male_blue_gray";
                break;

            case "A4":
                image = "user_male_blue_red";
                break;

            case "A5":
                image = "user_male_blue_yellow";
                break;

            case "G1":
                image = "user_male_green_black";
                break;

            case "G2":
                image = "user_male_green_brown";
                break;

            case "G3":
                image = "user_male_green_gray";
                break;

            case "G4":
                image = "user_male_green_red";
                break;

            case "G5":
                image = "user_male_green_yellow";
                break;

            case "R1":
                image = "user_male_red_black";
                break;

            case "R2":
                image = "user_male_red_brown";
                break;

            case "R3":
                image = "user_male_red_gray";
                break;

            case "R4":
                image = "user_male_red_red";
                break;

            case "R5":
                image = "user_male_red_yellow";
                break;
        }
        return image;
    }

    //Obtener nombre de imagen de mujer
    public String ObtenerImagenMujer(String nombre){
        String image = "";
        switch (nombre){
            case "A1":
                image = "user_female_blue_black";
                break;

            case "A2":
                image = "user_female_blue_brown";
                break;

            case "A3":
                image = "user_female_blue_gray";
                break;

            case "A4":
                image = "user_female_blue_red";
                break;

            case "A5":
                image = "user_female_blue_yellow";
                break;

            case "G1":
                image = "user_female_green_black";
                break;

            case "G2":
                image = "user_female_green_brown";
                break;

            case "G3":
                image = "user_female_green_gray";
                break;

            case "G4":
                image = "user_female_green_red";
                break;

            case "G5":
                image = "user_female_green_yellow";
                break;

            case "P1":
                image = "user_female_purple_black";
                break;

            case "P2":
                image = "user_female_purple_brown";
                break;

            case "P3":
                image = "user_female_purple_gray";
                break;

            case "P4":
                image = "user_female_purple_red";
                break;

            case "P5":
                image = "user_female_purple_yellow";
                break;
        }
        return image;
    }
}
